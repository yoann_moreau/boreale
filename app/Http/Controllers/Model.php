<?php


// account

function getId($id, $type = null) { // switch between id_user and the other one

	if ($type === null) { // get id by id_user

		$sql = "SELECT patients.id_patient FROM patients WHERE patients.id_user = '$id'";
		$result = \DB::select($sql);
		if (empty($result)) {
			$sql = "SELECT practitioners.id_practitioner FROM practitioners WHERE practitioners.id_user = '$id'";
			$result = \DB::select($sql);
			if (empty($result)) {
				$sql = "SELECT admins.id_admin FROM admins WHERE admins.id_user = '$id'";
				$result = \DB::select($sql);
				if (empty($result))
					return false;
				else
					$result = $result[0] -> id_admin;
			}
			else
				$result = $result[0] -> id_practitioner;
		}
		else
			$result = $result[0] -> id_patient;
	}
	else { // get id_user by id

		$table = $type.'s';
		$id0 = 'id_'.$type;

		$sql = "SELECT $table.id_user FROM $table WHERE $id0 = '$id'";
		$result = \DB::select($sql);
		if (empty($result))
			return false;
	$result = $result[0] -> id_user;
	}

	return $result;
}

function getIdByMail($mail) {
	$sql = "SELECT id_user FROM users WHERE email = '$mail'";
	$id = \DB::select($sql)[0] -> id_user;

	return $id;
}

function idPractitioner($mail) {
	$sql = "SELECT id_practitioner FROM practitioners WHERE email = '$mail'";
	$id = \DB::select($sql)[0];

	return $id;
}

function exist($mail) {
  $sql = "SELECT users.email FROM users WHERE users.email = '$mail'";
	$result = \DB::select($sql);
	return (!empty($result));
}

function login($email, $password) {
  $sql = "SELECT email, firstname, password, validated, account_type FROM users WHERE users.email = '$email'";
	$result = \DB::select($sql);

	if (!empty($result)) {
		$rValidated = $result[0]->validated;
		$rPassword = $result[0]->password;
		$type = $result[0]->account_type;
		$fname = $result[0]->firstname;

		if (($rValidated == 1) && password_verify($password, $rPassword)) {

			$super = false;
			if ($type == 'admin') {
				$sql = "SELECT id_admin FROM admins WHERE admins.email = '$email'";
				$id = \DB::select($sql);
				$id = $id[0]->id_admin;
				if ($id == 1)
					$super = true;
			}
			return ['type' => $type, 'fname' => $fname, 'super' => $super];

		}
	}
	return false;
}

function displayAdmin(){
	$sql ="SELECT DISTINCT id_admin, firstname, name, id_user FROM admins";
	$result = \DB::select($sql);

	return $result;
}

function removeAdmin($idA, $idU){
	$sql ="DELETE FROM admins WHERE admins.id_admin = '$idA'";
	\DB::delete($sql);

	$sql ="DELETE FROM users WHERE users.id_user = '$idU'";
	\DB::delete($sql);
}

function insertAccount($type, $mail, $password, $fname, $lname, $random, $address = null, $zcode = null, $city = null) {

	$sql = "INSERT INTO users VALUES (NULL, '$mail', '$fname', '$lname', '$password', 0, '$random', '$type')";
	\DB::insert($sql);
	$sql = "SELECT users.id_user FROM users WHERE email = '$mail'";
  $result = \DB::select($sql);
	$id = $result[0]->id_user;

	if ($type === 'patient') {
		$sql = "INSERT INTO patients VALUES (NULL, '$mail', '$fname', '$lname', '$password', '$address', '$zcode', '$city', 0, 0, $id)";
		\DB::insert($sql);

	} else if ($type === 'practitioner') {
		$sql = "INSERT INTO practitioners VALUES (NULL, '$mail', '$fname', '$lname', '$password', '$address', '$zcode', '$city', 0, 0, 0, 0, '', 0, $id)";
		\DB::insert($sql);
	} else if ($type === 'admin') {
			$sql = "UPDATE users SET validated = 1 WHERE email = '$mail'";
			\DB::update($sql);

			$sql = "INSERT INTO admins VALUES (NULL, '$mail', '$fname', '$lname', '$password', $id)";
			\DB::insert($sql);
		}
}

function updateLoc($type, $mail, $lat, $lon) {
	if ($type === 'patient') {
		$sql = "UPDATE patients SET latitude = '$lat', longitude = '$lon' WHERE
			email = '$mail'";
	} elseif ($type === 'practitioner') {
		$sql = "UPDATE practitioners SET latitude = '$lat', longitude = '$lon' WHERE
			email = '$mail'";
	}
	\DB::update($sql);
}

function validate($link) {
  $sql = "SELECT email, validated, account_type FROM users WHERE users.link = '$link'";
	$result = \DB::select($sql);

  if (!empty($result)) {
		$mail = $result[0]->email;
		$type = $result[0]->account_type;

    $sql = "UPDATE users SET users.validated = 1 WHERE users.link = '$link'";
		\DB::update($sql);
    $sql = "UPDATE users SET users.link = 0 WHERE users.link = '$link'";
		\DB::update($sql);
  }
  return false;
}

function recoverPassByMail($mail, $link) {
	$sql = "SELECT users.id_user FROM users WHERE email = '$mail'";
  $result = \DB::select($sql);
	if (empty($result))
		return false;

	$sql = "UPDATE users SET users.link = '$link' WHERE users.email = '$mail'";
	\DB::update($sql);

	return true;
}

function testLink($link) {
	$sql = "SELECT users.link FROM users WHERE users.link = '$link'";
	$result = \DB::select($sql);

	if (empty($result))  // if link doesn't exist
		return false;

	$result = $result[0] -> link;

	return (strlen($result) > 30);  //prevent user from use the overwriting link '0'
}

function modifyPassword($link, $password) {

	// replace lost password by the new one
	$sql = "UPDATE users SET users.password = '$password' WHERE users.link = '$link'";
	\DB::update($sql);

	//overwrite link
	$sql = "UPDATE users SET users.link = '0' WHERE users.link = '$link'";
	\DB::update($sql);
}


// Search

function getPatientLoc($mail) {
	$sql = "SELECT latitude, longitude FROM patients WHERE email = '$mail'";
	$loc = \DB::select($sql)[0];

	return $loc;
}

function getPatientAddress($mail) {
	$sql = "SELECT address, zip, city FROM patients WHERE email = '$mail'";
	$address = \DB::select($sql)[0];

	return $address;
}

function getPractitionersLoc() {
	$sql = 'SELECT id_practitioner, firstname, name, radius, latitude, longitude
		FROM practitioners';
	$practitioners = \DB::select($sql);

	return $practitioners;
}

function getPractitionerServices($id) {
	$sql = "SELECT title FROM services
		JOIN have_services ON have_services.id_service = services.id_service
		WHERE have_services.id_practitioner = '$id'";
	$services = \DB::select($sql);

	return $services;
}


// Appointments

function getAppointsPract($id) {
	$sql = "SELECT * FROM appointments WHERE appointments.id_practitioner = '$id'";
	$result = \DB::select($sql);
	$list =  [];
	foreach ($result as $appointment) { // make an array of appoints, and each appoint is an array of elements
		$one = [];
		array_push($one, $appointment -> id_appointment);
		array_push($one, $appointment -> status);
		array_push($one, $appointment -> date);
		array_push($one, $appointment -> duration);
		array_push($one, $appointment -> amount);

		$idPract = $appointment -> id_practitioner;

		array_push($one, $appointment -> idPract);

		array_push($list, $one);

	}
	return $list;
}

function getAppointsPat($id) {
	$sql = "SELECT * FROM appointments WHERE appointments.id_patient = '$id'";
	$result = \DB::select($sql);
	$list =  [];
	foreach ($result as $appointment) { // make an array of appoints, and each appoint is an array of elements
		$one = [];
		array_push($one, $appointment -> status);
		array_push($one, $appointment -> date);
		array_push($one, $appointment -> duration);
		array_push($one, $appointment -> amount);

		// get practitioner name
		$idPract = $appointment -> id_practitioner;
		$sql = "SELECT firstname, name FROM practitioners WHERE practitioners.id_practitioner = '$idPract'";
		$resultName = \DB::select($sql)[0];
		$fname = $resultName -> firstname;
		$lname = $resultName -> name;
		$practName = $fname.' '.$lname;
		array_push($one, $practName);

		// get services for the current appointment


		// array_push($one, $services);

		array_push($list, $one);

	}
	return $list;
}

function getUnavailablesPract($idPract, $idPat) {
	$sql = "SELECT date, duration FROM appointments WHERE appointments.id_practitioner = '$idPract' AND appointments.id_patient != '$idPat'";
	$result = \DB::select($sql);
	$list =  [];

	foreach ($result as $appointment) { // make an array of appoints, and each appoint is an array of elements (date & duration)
		$one = [];
		array_push($one, $appointment -> date);
		array_push($one, $appointment -> duration);

		array_push($list, $one);

	}
	return $list;
}

//function removeExpired() {
//	$sql = "DELETE FROM appointments WHERE appointments.status = 1";
//	\DB::delete($sql);
//
//	// TO DO : add request for 'appointments_have_services'
//}


// mails

function getMail($type) {
	$sql = "SELECT mails.subject, mails.message FROM mails WHERE mails.type = '$type'";
	$result = \DB::select($sql)[0];

	return ['object' => $result -> subject, 'message' => $result -> message];
}

function mailUpdate($type, $object, $message) {
	$sql = "UPDATE mails SET mails.subject = '$object', mails.message = '$message' WHERE mails.type = '$type'";
	\DB::update($sql);
	return 1;
}


// texts

function getTexts($page) {

	//get texts of current page
	$sql = "SELECT texts.text FROM texts WHERE texts.page = '$page'";
	$result = \DB::select($sql);

	$list = [];
	for ($i = 0; $i < count($result); $i++) {
		array_push($list, $result[$i]->text);
	}
	return $list;
}

function updateText($text, $page, $title) {
	$sql = "UPDATE texts SET texts.text = '$text' WHERE texts.page = '$page' AND texts.title = '$title'";
	\DB::update($sql);
}


// services

function services() {
	$sql = "SELECT id_service, title, price, description, picture FROM services ORDER BY title";
	$services = \DB::select($sql);

	return $services;
}

function service($id) {
	$sql = "SELECT id_service, title, price, description, picture FROM services WHERE id_service = '$id'";
	$service = \DB::select($sql);

	return $service[0];
}

function insertService($title, $price, $desc, $pic) {
	$sql = "INSERT INTO services VALUES (NULL, '$title', '$price', '$desc',	'$pic')";
	\DB::insert($sql);
}

function updateService($id, $title, $price, $desc, $pic) {
	$sql = "UPDATE services SET title = '$title', price = '$price',
		description = '$desc', picture = '$pic' WHERE id_service = '$id'";
	\DB::update($sql);
}

function deleteService($id) {
	$sql = "DELETE FROM services WHERE id_service = '$id'";
	\DB::delete($sql);
}

function serviceTitle($title) {
	$sql = "SELECT title FROM services WHERE title = '$title'";
	$result = \DB::select($sql);

	if (empty($result)) {
		return false;
	} else {
		return true;
	}
}

function deselectServices($id_practitioner) {
	$sql = "DELETE FROM have_services WHERE id_practitioner = '$id_practitioner'";
	\DB::delete($sql);
}

function selectService($id_service, $id_practitioner) {
	$sql = "INSERT INTO have_services VALUES ('$id_service', '$id_practitioner')";
	\DB::insert($sql);
}

function haveServices($id_practitioner) {
	$sql = "SELECT id_service, id_practitioner FROM have_services WHERE
		id_practitioner = '$id_practitioner'";
	$result = \DB::select($sql);

	return $result;
}


// informations

function informations($mail, $type) {

	if ($type == 'patient') {
		$sql = "SELECT firstname, name, email, address, zip, city FROM patients WHERE patients.email = '$mail'";
		$result =\DB::select($sql);

		$fname = $result[0]->firstname;
		$name = $result[0]->name;
		$email = $result[0]->email;
		$address = $result[0]->address;
		$zip = $result[0]->zip;
		$city = $result[0]->city;

		return ['fname' => $fname, 'name' => $name, 'email' => $email, 'address' => $address, 'zip' => $zip, 'city' => $city];
	}
	if ($type == 'practitioner') {
		$sql = "SELECT * FROM practitioners WHERE practitioners.email = '$mail'";
		$result =\DB::select($sql);

		$fname = $result[0]->firstname;
		$name = $result[0]->name;
		$email = $result[0]->email;
		$address = $result[0]->address;
		$zip = $result[0]->zip;
		$city = $result[0]->city;
		$radius = $result[0]->radius;
		$siret = $result[0]->siret;
		$iban = $result[0]->IBAN;
//		$file = $result[0]->file;

		return ['fname' => $fname, 'name' => $name, 'email' => $email, 'address' => $address, 'zip' => $zip, 'city' => $city, 'radius' => $radius, 'siret' => $siret, 'iban' => $iban];
	}
	if ($type == 'admin') {
		$sql = "SELECT firstname, name, email FROM admins WHERE admins.email = '$mail'";
		$result =\DB::select($sql);

		$fname = $result[0]->firstname;
		$name = $result[0]->name;
		$email = $result[0]->email;

		return ['fname' => $fname, 'name' => $name, 'email' => $email];
	}

	return false;
}

function infoUpdate($type, $fname, $name, $oldmail, $email, $address, $zip, $city, $radius, $siret, $iban){

	if ($type === 'practitioner') {
		$sql = "UPDATE practitioners SET practitioners.firstname = '$fname', practitioners.name = '$name', practitioners.email = '$email', practitioners.address = '$address', practitioners.zip = '$zip', practitioners.city = '$city', practitioners.radius = '$radius', practitioners.siret = '$siret', practitioners.IBAN = '$iban' WHERE practitioners.email='$oldmail' ";
		\DB::update($sql);
	}

	if ($type === 'patient') {
		$sql = "UPDATE patients SET patients.firstname = '$fname', patients.name = '$name', patients.email = '$email', patients.address = '$address', patients.zip = '$zip', patients.city = '$city' WHERE patients.email = '$oldmail'";
		\DB::update($sql);
	}
	if ($type === 'admin') {
		$sql = "UPDATE admins SET admins.firstname = '$fname', admins.name = '$name', admins.email = '$email' WHERE admins.email = '$oldmail'";
		\DB::update($sql);
	}

	$sql = "UPDATE users SET users.firstname = '$fname', users.name = '$name', users.email = '$email' WHERE users.email = '$oldmail'";
	\DB::update($sql);

}

function verifyPassword($email) {
	$sql = "SELECT password FROM users WHERE users.email = '$email'";
	$result = \DB::select($sql);

	$rPassword = $result[0]->password;
	return $rPassword;
}

function updatePsw($type, $email, $cpassword){

		if ($type === 'practitioner') {
			$sql = "UPDATE practitioners SET practitioners.password = '$cpassword' WHERE practitioners.email='$email' ";
			\DB::update($sql);
		}

		if ($type === 'patient') {
			$sql = "UPDATE patients SET patients.password = '$cpassword' WHERE patients.email = '$email'";
			\DB::update($sql);
		}
		if ($type === 'admin') {
			$sql = "UPDATE admins SET admins.password = '$cpassword' WHERE admins.email = '$email'";
			\DB::update($sql);
		}

		$sql = "UPDATE users SET users.password = '$cpassword' WHERE users.email = '$email'";
		\DB::update($sql);
}
