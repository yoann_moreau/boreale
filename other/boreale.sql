-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 10 fév. 2019 à 20:51
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `boreale`
--

-- --------------------------------------------------------

--
-- Structure de la table `admins`
--

CREATE TABLE `admins` (
  `id_admin` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `admins`
--

INSERT INTO `admins` (`id_admin`, `email`, `firstname`, `name`, `password`, `id_user`) VALUES
(1, 'admin.admin@gmail.com', 'Prénom', 'Nom', '$2y$10$e5Qnx6CxmUoPSlCB8y4BqO2wkgAk22B.0.X46O.2WAUaWF7H0lxKG', 1);

-- --------------------------------------------------------

--
-- Structure de la table `aftercares`
--

CREATE TABLE `aftercares` (
  `id_message` int(11) NOT NULL,
  `message` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `id_patient` int(11) NOT NULL,
  `id_practitioner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `appointments`
--

CREATE TABLE `appointments` (
  `id_appointment` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `duration` tinyint(1) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `id_payment` int(11) NOT NULL,
  `id_patient` int(11) NOT NULL,
  `id_practitioner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `appointments`
--

INSERT INTO `appointments` (`id_appointment`, `status`, `date`, `duration`, `amount`, `id_payment`, `id_patient`, `id_practitioner`) VALUES
(2, 1, '2019-02-12 15:00:00', 2, '40', 1, 1, 1),
(4, 2, '2019-02-14 09:00:00', 1, '30', 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `appointments_have_services`
--

CREATE TABLE `appointments_have_services` (
  `id_service` int(11) NOT NULL,
  `id_appointment` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `global`
--

CREATE TABLE `global` (
  `invoice_date` int(11) NOT NULL,
  `percentage` decimal(10,0) NOT NULL,
  `subscription` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `have_services`
--

CREATE TABLE `have_services` (
  `id_service` int(11) NOT NULL,
  `id_practitioner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `have_services`
--

INSERT INTO `have_services` (`id_service`, `id_practitioner`) VALUES
(1, 1),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `mails`
--

CREATE TABLE `mails` (
  `type` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `mails`
--

INSERT INTO `mails` (`type`, `subject`, `message`) VALUES
('patient-validation', 'Boréale: création de compte', 'bonjour, nous sommes très heureux de vous compter parmi nous.'),
('practitioner-validation', 'Boréale: création de compte', 'bonjour, nous sommes très heureux de vous compter à présent parmi nous'),
('admin-validation', 'Boréale: création de compte', 'bonjour, vous avez été choisi pour rejoindre léquipe de Boréale. nous sommes très heureux de vous compter à présent parmi nous. Cordialement, le super administrateur.'),
('contact', 'Boréale: contact', 'Nous avons bien reçu votre message, et nous nous efforcerons de vous donner une réponse dans les plus bref délais.\nAu revoir'),
('lost-password', 'mot de passe perdu', 'Vous avez demandé la réinitialisation du mot de passe de votre compte Boréale associé à cette adresse mail.\nCliquez sur le lien suivant ou copiez-le dans votre navigateur pour finir la démarche.');

-- --------------------------------------------------------

--
-- Structure de la table `patients`
--

CREATE TABLE `patients` (
  `id_patient` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `patients`
--

INSERT INTO `patients` (`id_patient`, `email`, `firstname`, `name`, `password`, `address`, `zip`, `city`, `latitude`, `longitude`, `id_user`) VALUES
(1, 'patient.patient@gmail.com', 'Jean-Hipogène', 'Eude', '$2y$10$whSZ5P4mxaO1Br/Uqgs1fOqKu7.RIE4qRgQTuKCXj9lWnR3Un/q4y', '30 rue du midi', '58000', 'Nevers', 0, 0, 2);

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

CREATE TABLE `payments` (
  `id_payment` int(11) NOT NULL,
  `reference` int(11) NOT NULL,
  `amount` decimal(8,0) NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `method` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `registery_date` datetime NOT NULL,
  `payment_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `payments`
--

INSERT INTO `payments` (`id_payment`, `reference`, `amount`, `status`, `method`, `registery_date`, `payment_date`) VALUES
(1, 4, '30', 'en attente', 'par chèque', '2019-01-29 16:34:57', '2019-01-29 16:34:57');

-- --------------------------------------------------------

--
-- Structure de la table `practitioners`
--

CREATE TABLE `practitioners` (
  `id_practitioner` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `radius` int(10) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `siret` varchar(14) COLLATE utf8_unicode_ci NOT NULL,
  `certificate` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `IBAN` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `practitioners`
--

INSERT INTO `practitioners` (`id_practitioner`, `email`, `firstname`, `name`, `password`, `address`, `zip`, `city`, `radius`, `latitude`, `longitude`, `siret`, `certificate`, `IBAN`, `id_user`) VALUES
(1, 'praticien@gmail.com', 'Prénom', 'Nom', '$2y$10$P.9MELLbtcoR2slao7XdtOPVhkhKmQB4U36wWX2mOnI8b3ANq6Byy', '2, rue du 14 juillet', '58000', 'Nevers', 25, 46.9882, 3.15722, '54206547900926', '', 'FR7614410000011234567890163', 4);

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `id_service` int(11) NOT NULL,
  `title` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `price` decimal(8,0) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`id_service`, `title`, `price`, `description`, `picture`) VALUES
(1, 'Massage', '500', 'non', ''),
(2, 'service', '30', 'un service très intéressant', ''),
(3, 'perruque', '2000', 'une perruuuuque', '');

-- --------------------------------------------------------

--
-- Structure de la table `texts`
--

CREATE TABLE `texts` (
  `id_text` int(11) NOT NULL,
  `title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(32) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `texts`
--

INSERT INTO `texts` (`id_text`, `title`, `text`, `page`) VALUES
(3, '0', 'Estime-toi !', 'home'),
(4, '1', 'La beauté n\'est pas futile, elle est vitale.\r\nVitale pour affronter les traitements. \r\nVitale pour guérir.\r\nBoréale vous met en relation avec des socio-esthéticiennes \r\nqui vont aideront à retrouver votre identité \r\net votre confiance en vous.', 'home'),
(5, '2', 'Boréale vous propose', 'home'),
(6, '3', 'Des prestations esthétiques adaptées à vos besoins, à votre condition, à votre maladie avec des socio-esthéticiennes diplomées.', 'home'),
(7, '4', 'Des séances particulières avec un praticien \r\nde l\'esthétique réapprendre \r\nà prendre soin de vous et composer  \r\navec la nouvelle version de votre corps.', 'home'),
(8, '5', 'Des ateliers pour apprendre, en groupe, à camoufler les vestiges de la maladie.', 'home'),
(9, '6', 'Des produits pensés pour les corps éprouvés', 'home'),
(11, '0', 'Qualité, Sécurité, Compétence', 'patient-infos'),
(12, '1', 'Nos praticiennes sont toutes diplômées\r\nd’État et ont reçu une formation de\r\nspécialisation en socio-esthétique ou\r\nonco-esthétique.', 'patient-infos'),
(13, '2', 'Elles s’engagent à respecter une charte\r\nd’éthique et un contrat de confidentialité \r\nprotégeant vos informations privées.', 'patient-infos'),
(14, '3', 'Les praticiennes qui travaillent avec Boréale s\'investissent de façon constante, se forment aux dernières techniques et nouveautés en matière de socio-esthétique.', 'patient-infos'),
(15, '4', 'Avec Boréale pas d\'échange d\'argent avec\r\nla praticienne. Vous payez en ligne, ce qui\r\nlibère votre relation de confiance avec\r\nvotre accompagnatrice.\r\nVous pouvez régler en carte bancaire ou\r\nutiliser vos CESU.\r\nUne autorisation d\'agrément Sécurité\r\nSociale est encours.', 'patient-infos'),
(17, '0', 'Boréale réunit l\'humain et la confiance en soi', 'practitioner-infos'),
(18, '1', 'Vous avez une vocation, Boréale vous offre\r\nun revenu.\r\nVous entrez en relation avec des personnes\r\nqui ont besoin de vous.', 'practitioner-infos'),
(19, '2', 'Boréale vous permet d\'organiser votre emploi du temps et d\'automatiser la prise de rendez-vous de vos clients.\r\nSa plateforme de paiement,sécurise votre rémunération.', 'practitioner-infos'),
(20, '3', 'Vous pouvez échanger avec la communauté de socio-esthéticiennes sur nos réseaux sociaux et partager vos réalisations grâce aux témoignages du bien-être apporté à vos clients.', 'practitioner-infos'),
(24, '0', 'Bienvenue dans votre profil, ici vous pouvez faire pleinde choses très intéressantes', 'profile'),
(33, '0', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor', 'about-us'),
(34, '1', 'Qui sommes-nous ?', 'about-us'),
(35, '2', 'Découvrir Boréale', 'about-us'),
(36, '3', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(37, '4', 'Boréale vous propose', 'about-us'),
(38, '5', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(39, '6', 'Nos valeurs et engagements', 'about-us'),
(40, '7', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.', 'about-us'),
(41, '0', 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium', 'legal'),
(42, '1', 'Mentions Légales', 'legal'),
(43, '2', 'Sed ut perspiciatis unde omnis ', 'legal'),
(44, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'legal'),
(45, '4', 'Sed ut perspiciatis unde omnis ', 'legal'),
(46, '5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'legal'),
(48, '0', 'At vero eos et accusamus et iusto odio dignissimos ducimus', 'partner'),
(49, '1', 'Nos partenaires', 'partner'),
(50, '0', 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates', 'faq'),
(51, '1', 'Foire Aux Questions', 'faq'),
(52, '0', 'Nous contacter', 'contact'),
(53, '2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ?', 'faq'),
(54, '3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(55, '4', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ?', 'faq'),
(56, '5', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(57, '6', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit ?', 'faq'),
(58, '7', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(59, '8', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ?', 'faq'),
(60, '9', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(61, '10', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do?', 'faq'),
(62, '11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum', 'faq'),
(63, '0', 'Bienvenue dans votre espace de recherche, ici vous trouverez les prestations de vos choix', 'prestations'),
(64, '1', 'Trouver votre prestation', 'prestations'),
(65, '1', 'Votre profil', 'profile'),
(66, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'patient-area'),
(67, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'practitioner-area'),
(68, '0', 'Bienvenue dans votre espace, ici vous trouverez tout ce dont vous avez besoin', 'admin-area'),
(69, '0', 'Bienvenue dans votre agenda, ici vous pouvez visualiser vos prochains rendez-vous', 'agenda'),
(70, '1', 'Vos rendez-vous', 'agenda'),
(71, '2', 'Nom du partenaire', 'partner'),
(72, '3', 'Présentation du partenaire : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.', 'partner'),
(73, '0', 'Bienvenue dans votre espace d\'édition de pages, ici vous pouvez accéder aux pages du site dont vous souhaitez modifier le contenu', 'edition'),
(74, '0', 'Bienvenue dans votre espace de gestion de comptabilité', 'practitioner-accounting'),
(75, '0', 'Bienvenue dans votre espace de gestion de vos prestations', 'practitioner-services');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `validated` tinyint(1) NOT NULL,
  `link` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `account_type` varchar(16) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id_user`, `email`, `firstname`, `name`, `password`, `validated`, `link`, `account_type`) VALUES
(1, 'admin.admin@gmail.com', 'Prénom', 'Nom', '$2y$10$OBQ7oDyVR2qecBZJzWho0uBOxt0oWHcpx/lsnZYyeqlNUMdyRdtd2', 1, 'i5kmc7jd8b5d2gzyq5wciep2q7wxbps8', 'admin'),
(2, 'patient.patient@gmail.com', 'Jean-Hipogène', 'Eude', '$2y$10$whSZ5P4mxaO1Br/Uqgs1fOqKu7.RIE4qRgQTuKCXj9lWnR3Un/q4y', 1, '2snk0xl47qs711zmb82un0z1nwbege4q', 'patient'),
(4, 'praticien@gmail.com', 'Prénom', 'Nom', '$2y$10$P.9MELLbtcoR2slao7XdtOPVhkhKmQB4U36wWX2mOnI8b3ANq6Byy', 1, '0', 'practitioner');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_admin`);

--
-- Index pour la table `aftercares`
--
ALTER TABLE `aftercares`
  ADD PRIMARY KEY (`id_message`),
  ADD KEY `aftercares_patients_FK` (`id_patient`),
  ADD KEY `aftercares_practitioners0_FK` (`id_practitioner`);

--
-- Index pour la table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`id_appointment`),
  ADD KEY `appointments_payments_FK` (`id_payment`),
  ADD KEY `appointments_patients0_FK` (`id_patient`),
  ADD KEY `appointments_practitioners1_FK` (`id_practitioner`);

--
-- Index pour la table `appointments_have_services`
--
ALTER TABLE `appointments_have_services`
  ADD PRIMARY KEY (`id_service`,`id_appointment`),
  ADD KEY `appointments_have_services_appointments0_FK` (`id_appointment`);

--
-- Index pour la table `have_services`
--
ALTER TABLE `have_services`
  ADD PRIMARY KEY (`id_service`,`id_practitioner`),
  ADD KEY `have_services_practitioners0_FK` (`id_practitioner`);

--
-- Index pour la table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id_patient`);

--
-- Index pour la table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id_payment`);

--
-- Index pour la table `practitioners`
--
ALTER TABLE `practitioners`
  ADD PRIMARY KEY (`id_practitioner`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id_service`);

--
-- Index pour la table `texts`
--
ALTER TABLE `texts`
  ADD PRIMARY KEY (`id_text`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `admins`
--
ALTER TABLE `admins`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `aftercares`
--
ALTER TABLE `aftercares`
  MODIFY `id_message` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `id_appointment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `patients`
--
ALTER TABLE `patients`
  MODIFY `id_patient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `payments`
--
ALTER TABLE `payments`
  MODIFY `id_payment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `practitioners`
--
ALTER TABLE `practitioners`
  MODIFY `id_practitioner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `services`
--
ALTER TABLE `services`
  MODIFY `id_service` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `texts`
--
ALTER TABLE `texts`
  MODIFY `id_text` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `aftercares`
--
ALTER TABLE `aftercares`
  ADD CONSTRAINT `aftercares_patients_FK` FOREIGN KEY (`id_patient`) REFERENCES `patients` (`id_patient`),
  ADD CONSTRAINT `aftercares_practitioners0_FK` FOREIGN KEY (`id_practitioner`) REFERENCES `practitioners` (`id_practitioner`);

--
-- Contraintes pour la table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `appointments_patients0_FK` FOREIGN KEY (`id_patient`) REFERENCES `patients` (`id_patient`),
  ADD CONSTRAINT `appointments_payments_FK` FOREIGN KEY (`id_payment`) REFERENCES `payments` (`id_payment`),
  ADD CONSTRAINT `appointments_practitioners1_FK` FOREIGN KEY (`id_practitioner`) REFERENCES `practitioners` (`id_practitioner`);

--
-- Contraintes pour la table `appointments_have_services`
--
ALTER TABLE `appointments_have_services`
  ADD CONSTRAINT `appointments_have_services_appointments0_FK` FOREIGN KEY (`id_appointment`) REFERENCES `appointments` (`id_appointment`),
  ADD CONSTRAINT `appointments_have_services_services_FK` FOREIGN KEY (`id_service`) REFERENCES `services` (`id_service`);

--
-- Contraintes pour la table `have_services`
--
ALTER TABLE `have_services`
  ADD CONSTRAINT `have_services_practitioners0_FK` FOREIGN KEY (`id_practitioner`) REFERENCES `practitioners` (`id_practitioner`),
  ADD CONSTRAINT `have_services_services_FK` FOREIGN KEY (`id_service`) REFERENCES `services` (`id_service`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
