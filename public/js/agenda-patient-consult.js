/*eslint-env browser*/

// Constantes

var oneDay = 86400000, // milliseconds in a day
		amplitude = 53 * 7 * oneDay, schedule = 11, hourStart = 8;

// Variables

var i, j, y0, date0, time, num, mon, delta;
var current = new Date(), today = current.getTime();

var listMonths = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'aôut', 'septembre', 'octobre', 'novembre', 'décembre'];
var listDays = ['lun.', 'mar.', 'mer.', 'jeu.', 'ven.', 'sam.', 'dim.'];
var weekNums, appointments, hours;


// Code to execute

getAppointments();


// Functions

function getAppointments() { // get appointments informations from database

	var request;
	if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
	else
		request = new ActiveXObject('Microsoft.XMLHTTP');

	request.open('POST', '/get-appoints-pat', true);
	request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
	request.responseType = 'text';

	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			appointments = JSON.parse(this.responseText);
			newCalendar(current);
		}
		if (this.readyState == 4 && this.status != 200) {
			alert('erreur '+ this.status);
		}
	}
	request.send();
}

function newCalendar(date) { // calcul and show new calendar
	mon = monday(date);
	setWeekNums(mon);
	displayDate(mon);
	displayWeek();
	displayCalendar();
	displayAppointments();
	click();
}

function stringDate(element) {  // cast string in date, or date in string
	switch (typeof(element)) {
		case 'string' :
			var tab = element.split(' '); // split date & hour
			var tabD = tab[0]; // date
			var tabH = tab[1]; // hour
			tabD = tabD.split('-'); // split year, month & day
			tabH = tabH.split(':'); // split hours, minutes & seconds
			return new Date(parseInt(tabD[0]), parseInt(tabD[1])-1, parseInt(tabD[2]), parseInt(tabH[0]));

		case 'object' :
			return element.getFullYear().toString() +'-'
				+ (element.getMonth()+1).toString() +'-'
				+element.getDate().toString() +' '
				+element.getHours().toString() +':00:00';

		default :
			return false;
	}
}

function equal(date1, date2) { // check if two date have same year, month, day and hour
	if (typeof(date1) === 'object' && typeof(date2) === 'object') {
		if (date1.getFullYear() === date2.getFullYear() && date1.getMonth() === date2.getMonth()
				&& date1.getDate() === date2.getDate() && date1.getHours() === date2.getHours()) {
			return true;
		}
		else {
			return false;
		}
	}

	return 'error: wrong type'; // if wrong setting are provided
}

function monday(date) {  //search date of weekstart

	time = date.getTime();

	do {
		date0 = new Date(time);
		time -= oneDay;
	}
	while (date0.getDay() !== 1);

	return date0;
}

function setWeekNums(date) {  // define the 7 numbers of days for selected week

	weekNums = [];
	time = date.getTime();
	for (i = 0; i < 7; i++) {
		num = new Date(time);
		weekNums.push(num);
		time += oneDay;
	}
}

function getWeek(date) {  // get rank of selected week in the year (betwwen 1 and 53)

	y0 = date.getFullYear(); // year of selected date

	// search first monday of the year
	i = 1;
	do {
		date0 = new Date(y0, 0, i);
		i++;
	}
	while (date0.getDay() !== 1);

	delta = (date.getTime() - date0.getTime());
	delta = Math.floor(delta/86400/7/1000) + 1;
	return delta;
}

function displayDate(date) {
	var y = date.getFullYear();
	var m = date.getMonth();
	var w = getWeek(mon);
	var theDate = y.toString() + ' - ' + listMonths[m]+' - semaine '+ w.toString();
	gEId('date').innerHTML = theDate;
}

function displayWeek() {  // display week's days, from monday to sunday
	var str = '';
	for (i = 0; i < 7; i++) {
		str += '<td>'+listDays[i]+'<br>'+weekNums[i].getDate()+'</td>';
		if (i == 6)
			str += '</tr>';
	}
	gEId('week').innerHTML = str;
}

function displayCalendar() {  // display calendar for selected date
	var str = '';
  for (i = 0; i < schedule; i++) {  // store date and hour data for each square
    str += '<tr>';
    for (j = 0; j < 7; j++) {
			var dataDate = weekNums[j];
			dataDate.setHours(hourStart+i);
			str += '<td data-date="'+stringDate(dataDate)+'"></td>';
      if (j == 6)
        str += '</tr>';
    }
  }
	gEId('calendar').innerHTML = str;
}

function displayAppointments() { // foreach square, check each hour of each appointment

	var squareDate, appointDate, appointDuration;

	hours = gEId('div-calendar').getElementsByTagName('td'); // check each square of the calendar
	var hoursLength = hours.length;
	for (i = 0; i < hoursLength ; i++) {
		squareDate = stringDate(hours[i].getAttribute('data-date'));
		var apptLength = appointments.length;
		for (j = 0; j < apptLength; j++) { // check each appointment
			appointDate = stringDate(appointments[j][1]);
			appointDuration = appointments[j][2];

			for (var k = 0; k < appointDuration; k++) { // check each hour of current appointment
				var appointDate0 = appointDate;
				appointDate0.setHours(appointDate.getHours()+k);

				if (equal(squareDate, appointDate0)) { // if one corresponds to one square of displayed week calendar
					hours[i].setAttribute('data-num', j);
					if (appointments[j][0] === 1)
						hours[i].classList.add('pending');
					if (appointments[j][0] === 2)
						hours[i].classList.add('appoint');

				}
			}
		}
	}
}

function displayAppointInfos(hour) {
	var num = hour.getAttribute('data-num');
	var appoint = appointments[num];
	gEClass('overlay')[0].classList.add('visible'); // display overlay

	var infoStatus = appoint[0];
	var infoDate = appoint[1];
	var infoDuration = appoint[2];
	var infoAmount = appoint[3];
	var infoPract = appoint[4];

	var infoHour = formateInfo('hour', [infoDate, infoDuration]); // define hours (date and duration required)
	infoDate = formateInfo('date', infoDate); // formate date
	infoStatus = formateInfo('status', infoStatus);

	var infos = '<p>'+infoDate+'</p>';  // display formated infos in overlay
	infos += '<p>'+infoHour+'</p>';
	infos += '<p>'+infoPract+'</p>';
	infos += '<p>'+infoAmount+' €</p>';
	infos += '<p>'+infoStatus+'</p>';
	gEClass('window')[0].innerHTML = infos;
}

function formateInfo(typeInfo, info) {
	var iDate;
	switch (typeInfo) {
		case 'date' :
			iDate = stringDate(info);
			var iDay = (iDate.getDate()).toString(); // day (string)
			var iMonth = (iDate.getMonth()); // month (int)
			var iYear = (iDate.getFullYear()).toString(); // year (string)
			var formatedDate = iDay +' '+listMonths[iMonth]+' '+iYear;
			return formatedDate;

		case 'hour' :
			iDate = stringDate(info[0]);
			var iDuration = info[1];
			var iHourStart = iDate.getHours();
			var iHourEnd = iHourStart + iDuration;
			var iHours = iHourStart.toString()+'h00 - '+iHourEnd.toString()+'h00';
			return iHours

		case 'status' :
			switch (info) {
				case 1 : return 'en attente de validation';
				case 2 : return 'validé';
			}
		break;

		default :
			return false;
	}
}


// Event listeners

gEId('previous').addEventListener('click', function() {
	time = current.getTime();
	time -= oneDay * 7;
	if (today - time <= amplitude) { // fix a limit in past
		current = new Date(time);
		newCalendar(current); // run functions with new selected week
	}
});

gEId('next').addEventListener('click', function() {
	time = current.getTime();
	time += oneDay * 7;
	if (time - today <= amplitude) { // fix a limit in future
		current = new Date(time);
		newCalendar(current); // run functions with new selected week
	}
});

gEClass('overlay')[0].addEventListener('click', function() {
	gEClass('overlay')[0].classList.remove('visible');
});

//gEId('make-appoint').addEventListener('click', function() { // valid appointement(s)
//
//});

function click() {
	var hoursLength = hours.length;
	for (i = 0; i < hoursLength; i++) {
		hours[i].addEventListener('click', function() { // when user click on an square

			switch(this.className) {
				case 'appoint':
					displayAppointInfos(this);
				break;
				case 'pending':
					displayAppointInfos(this);
				break;
				case 'unavailable':
					alert('unavailable');
				break;
			}
		});
	}
}
