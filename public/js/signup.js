/*eslint-env browser*/

// Variables
var accountType = gEName('account-type');
var submitBtn = gEName('submit-btn')[0];
var mail = gEName('mail')[0];
var password = gEName('password')[0];
var cPassword = gEName('confirm-password')[0];
var fname = gEName('firstname')[0];
var lname = gEName('lastname')[0];
var address =  gEName('address')[0];
var zipCode = gEName('zip-code')[0];
var city = gEName('city')[0];
var errors = 0;


// Functions

function validateEmail(email) {
	var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
	return re.test(email);
}

function validatePassword(pw) {
	var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-/:-?{-~!"^_`\[\]])(?=.{8,})/;
	return re.test(pw);
}

function validateString(str) {
	var re = /^(?=.*[A-Z])(?=.{2,})/i;
	return re.test(str);
}

function submitForm(e) {
	e.preventDefault();
	errors = 0;
	var errorMail = '';
	if (mail.value == '') {
		errorMail = 'Veuillez renseigner un email.';
	}
	else {
		if (validateEmail(mail.value) === false) {
			errorMail = 'Votre email n\'est pas valide.';
		}
		else existEmail();
	}
	gEId('error-mail').innerHTML = errorMail;

	var errorPW = '';
	if (validatePassword(password.value) === false) {
		errorPW = 'Votre mot de passe doit contenir au moins 8 caractères, ';
		errorPW += 'dont une minuscule, une majuscule, un chiffre et un symbole';
		errors++;
	}
	gEId('error-pw').innerHTML = errorPW;

	var errorCPW = '';
	if (cPassword.value !== password.value) {
		errorCPW = 'Vos mots de passe ne sont pas identiques.';
		errors++;
	}
	gEId('error-cpw').innerHTML = errorCPW;

	var errorFname = '';
	if (validateString(fname.value) === false || fname.value.length > 50) {
		errorFname = 'Prénom invalide.';
		errors++;
	}
	gEId('error-fname').innerHTML = errorFname;

	var errorLname = '';
	if (validateString(lname.value) === false || lname.value.length > 50) {
		errorLname = 'Nom invalide.';
		errors++;
	}
	gEId('error-lname').innerHTML = errorLname;

	var errorAddress = '';
	if (validateString(address.value) === false) {
		errorAddress = 'Format d\'adresse invalide.';
		errors++;
	}
	gEId('error-address').innerHTML = errorAddress;

	var errorZcode = '';
	if (zipCode.value.length !== 5) {
		errorZcode = 'Le code postal doit contenir 5 caractères.';
		errors++;
	}
	gEId('error-zcode').innerHTML = errorZcode;

	var errorCity = '';
	if (city.value.length < 2) {
		errorCity = 'Veuillez renseigner un nom de ville valide.';
		errors++;
	}
	gEId('error-city').innerHTML = errorCity;
}

function existEmail() {
	var request;
  if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
  else
    request = new ActiveXObject('Microsoft.XMLHTTP');

  request.open('POST', '/verification-email', true);
  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
  request.responseType = 'text';
  var data =
    'data-mail='+mail.value;
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = this.responseText;
      if (answer === 'true') {
				var errorMail = 'L\'adresse email est déjà utilisée';
				gEId('error-mail').innerHTML = errorMail;
//				gEId('alert1').classList.remove('hidden');
      }
      else {
//				gEId('alert1').classList.add('hidden');
				submitForm2();
      }
    }
    else if (this.readyState == 4 && this.status != 200) {
      alert('erreur '+this.status);
    }
  }
  request.send(data);
}

function submitForm2() {
	if (errors === 0) {
		gEId('account-creation').submit();
	}
}

function getToken() {
    var meta = gEName('csrf-token')[0];
    return meta.getAttribute('content');
}


// Event listeners

submitBtn.addEventListener('click', submitForm);

// Code to execute

