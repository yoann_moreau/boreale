/*eslint-env browser*/

// Variables

var accountType = gEName('account-type');
var email = gEName('email')[0];
var fname = gEName('fname')[0];
var lname = gEName('name')[0];
var address = gEName('address')[0];
var zipCode = gEName('zip')[0];
var city = gEName('city')[0];
var radius = gEName('radius')[0];
var siret = gEName('siret')[0];
var iban = gEName('iban')[0];
var errorSiret;
var errors;
var errorIban;

// Functions

String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.split(search).join(replacement);
};

function validatePassword(pw) {
	var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-/:-?{-~!"^_`\[\]])(?=.{8,})/;
	return re.test(pw);
}

function validateEmail(email) {
	var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
	return re.test(email);
}

function validateString(str) {
	var re = /^(?=.*[A-Z])(?=.{2,})/i;
	return re.test(str);
}

function validateForm(e) {
	e.preventDefault();
	errors = 0;

	var errorFname = '';
	if (validateString(fname.value) === false || fname.value.length > 50) {
		errorFname = 'Prénom invalide.';
		errors++;
	}
	gEId('error-fname').innerHTML = errorFname;

	var errorName = '';
	if (validateString(lname.value) === false || lname.value.length > 50) {
		errorName = 'Nom invalide.';
		errors++;
	}
	gEId('error-name').innerHTML = errorName;

	var errorAddress = '';
  if (document.body.contains(address)) {
		if (validateString(address.value) === false) {
			errorAddress = 'Format d\'adresse invalide.';
			errors++;
		}
		gEId('error-address').innerHTML = errorAddress;
	}

	var errorZcode = '';
  if (document.body.contains(zipCode)) {
		if (zipCode.value.length !== 5) {
			errorZcode = 'Le code postal doit contenir 5 caractères.';
			errors++;
		}
		gEId('error-zip').innerHTML = errorZcode;
	}

	var errorCity = '';
  if (document.body.contains(city)) {
    if (city.value.length < 2) {
      errorCity = 'Veuillez renseigner un nom de ville valide.';
      errors++;
    }
    gEId('error-city').innerHTML = errorCity;
  }

	var errorRadius = '';
	if (document.body.contains(radius)) {
		if (radius.value.length < 2 || radius.value.length > 3) {
			errorRadius = 'Rayon d\'action invalide.';
			errors++;
		}
		gEId('error-radius').innerHTML = errorRadius;
	}

	errorSiret = '';
	if (document.body.contains(siret)) {
		var siretValue = siret.value.replaceAll(' ', '');
		if (siretValue.length != 14) {
			errorSiret = 'Veuillez renseigner un numéro de Siret valide.';
			errors++;
		}
		else{
			isSiretValid(siretValue);
		}
		gEId('error-siret').innerHTML = errorSiret;
	}


	if (document.body.contains(iban)) {
		if (iban.value.length < 14 || iban.value.length > 35) {
			errorIban = 'Veuillez renseigner un numéro Iban valide.';
			errors++;
		}
		else{
			isValidIbanNumber(iban.value);
		}
		gEId('error-iban').innerHTML = errorIban;
    console.log(errorIban);
	}

	var errorMail = '';
	if (email.value == '') {
		errorMail = 'Veuillez renseigner un email.';
	} else {
		if (validateEmail(email.value) === false) {
			errorMail = 'Votre email n\'est pas valide.';
		} else {
			var currentMail = email.dataset.email;
			if (currentMail !== email.value) {
				existEmail();
			} else {
				submitForm();
			}
		}
	}
	gEId('error-mail').innerHTML = errorMail;

}

function isSiretValid(siret) {
	if ((siret.length != 14) || (isNaN(siret))) {
		errorSiret = 'Veuillez renseigner un numéro de Siret valide.';
		errors++;
	} else {
		// SIRET is a numeric of 14
		var somme = 0;
		var tmp;
		for (var cpt = 0; cpt < siret.length; cpt++) {
			if ((cpt % 2) == 0) { // impaire positions
				tmp = siret.charAt(cpt) * 2; // Multiply by 2
				if (tmp > 9)
					tmp -= 9; // If result > 9, we substract 9
			} else
				tmp = siret.charAt(cpt);
			somme += parseInt(tmp);
		}
		if ((somme % 10) != 0) { // If the result is multiple of 10, so Siret is valid
			errorSiret = "Veuillez renseigner un numéro de Siret valide.";
			errors++;
		}
	}
}

function mod97(string) {
	var checksum = string.slice(0, 2),
		fragment;
	for (var offset = 2; offset < string.length; offset += 7) {
		fragment = String(checksum) + string.substring(offset, offset + 7);
		checksum = parseInt(fragment, 10) % 97;
	}

  if (checksum === 1){
    errorIban = '';
    return 1;
  }
  else{
    errorIban = 'Veuillez renseigner un numéro de Iban valide.';
		errors++;
    console.log(errorIban);
  }
}

function isValidIbanNumber(input) {
	var CODE_LENGTHS = {
		AD: 24,
		AE: 23,
		AT: 20,
		AZ: 28,
		BA: 20,
		BE: 16,
		BG: 22,
		BH: 22,
		BR: 29,
		CH: 21,
		CR: 21,
		CY: 28,
		CZ: 24,
		DE: 22,
		DK: 18,
		DO: 28,
		EE: 20,
		ES: 24,
		FI: 18,
		FO: 18,
		FR: 27,
		GB: 22,
		GI: 23,
		GL: 18,
		GR: 27,
		GT: 28,
		HR: 21,
		HU: 28,
		IE: 22,
		IL: 23,
		IS: 26,
		IT: 27,
		JO: 30,
		KW: 30,
		KZ: 20,
		LB: 28,
		LI: 21,
		LT: 20,
		LU: 20,
		LV: 21,
		MC: 27,
		MD: 24,
		ME: 22,
		MK: 19,
		MR: 27,
		MT: 31,
		MU: 30,
		NL: 18,
		NO: 15,
		PK: 24,
		PL: 28,
		PS: 29,
		PT: 25,
		QA: 29,
		RO: 24,
		RS: 22,
		SA: 24,
		SE: 24,
		SI: 19,
		SK: 24,
		SM: 27,
		TN: 24,
		TR: 26
	};

	var iban = gEName('iban')[0].value.toUpperCase().replace(/[^A-Z0-9]/g, ''); // keep only alphanumeric characters
	var	code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/); // match and capture (1) the country code, (2) the check digits, and (3) the rest
	var	digits;
	// check syntax and length
	if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
		errorIban = 'Veuillez renseigner un numéro de Iban valide.';
		errors++;
	}
	// rearrange country code and check digits, and convert chars to ints
	digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
		return letter.charCodeAt(0) - 55;
	});
	// final check
	return mod97(digits);
}

function existEmail() {
	var request;
  if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
  else
    request = new ActiveXObject('Microsoft.XMLHTTP');

  request.open('POST', '/verification-email', true);
  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
  request.responseType = 'text';
  var data =
    'data-mail='+email.value;
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = this.responseText;
      if (answer === 'true') {
				var errorMail = 'L\'adresse email est déjà utilisée';
				gEId('error-mail').innerHTML = errorMail;
      }
      else {
				submitForm();
      }
    }
    else if (this.readyState == 4 && this.status != 200) {
      alert('erreur '+this.status);
    }
  }
  request.send(data);
}

function submitForm() {
	if (errors === 0) {
		var confirmationText = 'Modification(s) enregistrée(s)';
		gEId('confirmation-profile').innerHTML = confirmationText;
		gEId('profile-form').submit();
	}
}

function getToken() {
    var meta = gEName('csrf-token')[0];
    return meta.getAttribute('content');
}

function validatePasswords(){
	var oPass = gEName('password')[0];
	var nPass = gEName('new-password')[0];
	var cPass = gEName('confirm-password')[0];
	var errorPw = '';
	var errorNPsw = '';
	var errorFPsw = '';
  errors = 0;

	if(oPass.value === nPass.value){
		errorPw = 'Rentrez un mot de passe différent de l\'ancien.';
    errors++;
    console.log(errorPw);
  }
  gEId('error-pw').innerHTML = errorPw;

  if(validatePassword(cPass.value) === false){
		errorFPsw = 'Votre mot de passe doit contenir au moins 8 caractères, dont une majuscule, une minuscule, un chiffre et un symbole.';
    errors++;
  }
  gEId('error-cpw').innerHTML = errorFPsw;

  if(nPass.value !== cPass.value){
		errorNPsw = 'Vos mots de passe ne sont pas identiques.';
    errors++;
  }
  gEId('error-npw').innerHTML = errorNPsw;

  if (errors === 0){
		submitForm();
	}
}

function verifiatePass(event) {
	event.preventDefault();
	var request = new XMLHttpRequest();

	request.open('POST', '/ajaxChangePsw', true);
	request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
	request.responseType = 'text';
	var data = 'data-pass='+gEName('password')[0].value;
	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var answer = this.responseText;
      var errorOPsw = '';
			if (answer === 'false') {
				errorOPsw = 'Mot de passe non valide.';
        gEId('error-pw').innerHTML = errorOPsw;
			}
			else if (answer === 'true'){
				validatePasswords();
			}
		}
		else if (this.readyState == 4 && this.status != 200) {
			alert('erreur '+this.status);
		}
	}
  request.send(data);
}


// Event listeners

gEName('submit-btn')[0].addEventListener('click', validateForm);
gEName('submit-btn')[1].addEventListener('click', verifiatePass);

// Code to execute
