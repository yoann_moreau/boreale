/*eslint-env browser*/

// Variables
var mail = gEName('mail')[0];


// Functions

function validateEmail(email) {
	var re = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
	return re.test(email);
}

function submitForm(event) {
	event.preventDefault();
	if (mail.value == '') {
		gEId('error-mail').innerHTML = 'Veuillez renseigner un email.';
	}
	else {
		if (validateEmail(mail.value) === false)
			gEId('error-mail').innerHTML = 'Votre email n\'est pas valide.';
		else existEmail();
	}
}

function existEmail() {
	var request;
  if (window.XMLHttpRequest)
		request = new XMLHttpRequest();
  else
    request = new ActiveXObject('Microsoft.XMLHTTP');

  request.open('POST', './verification-email', true);
  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
  request.responseType = 'text';
  var data =
    'data-mail='+mail.value;

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var answer = this.responseText;
      if (answer === 'true') {
				gEId('forgot-password').submit();
      }
      else {
				gEId('error-mail').innerHTML = 'L\'adresse email saisie ne correspond à aucun compte';
      }
    }
    else if (this.readyState == 4 && this.status != 200) {
      alert('erreur '+this.status);
    }
  }
  request.send(data);
}

// Event listeners

gEName('submit-btn')[0].addEventListener('click', submitForm);

// Code to execute

