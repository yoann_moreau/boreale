/*eslint-env browser*/

// Variables
var submitBtn = gEName('submit-btn')[0];
var password = gEName('password')[0];
var cPassword = gEName('confirm-password')[0];

// Functions

function validatePassword(pw) {
	var re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$-/:-?{-~!"^_`\[\]])(?=.{8,})/;
	return re.test(pw);
}

function submitForm(event) {
	event.preventDefault();
	var errorPW = '';

	if (cPassword.value !== password.value) {
		gEId('error-cpw').innerHTML = 'Vos mots de passe ne sont pas identiques.';
	}
	else {
		if (validatePassword(password.value) === false) {
			errorPW = 'Votre mot de passe doit contenir au moins 8 caractères, ';
			errorPW += 'dont une minuscule, une majuscule, un chiffre et un symbole';
			gEId('error-cpw').innerHTML = errorPW;
		}
		else {
			gEId('change-pass').submit();
		}
	}
}


//function existEmail() {
//	var request;
//  if (window.XMLHttpRequest)
//		request = new XMLHttpRequest();
//  else
//    request = new ActiveXObject('Microsoft.XMLHTTP');
//
//  request.open('POST', './verification-email', true);
//  request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
//	request.setRequestHeader('X-CSRF-TOKEN', getToken());
//  request.responseType = 'text';
//  var data =
//    'data-mail='+mail.value;
//  request.onreadystatechange = function() {
//    if (this.readyState == 4 && this.status == 200) {
//      var answer = this.responseText;
//      if (answer === 'true') {
//				var errorMail = 'L\'adresse email est déjà utilisée';
//				gEId('error-mail').innerHTML = errorMail;
////				gEId('alert1').classList.remove('hidden');
//      }
//      else {
////				gEId('alert1').classList.add('hidden');
//				submitForm2();
//      }
//    }
//    else if (this.readyState == 4 && this.status != 200) {
//      alert('erreur '+this.status);
//    }
//  }
//  request.send(data);
//}

function submitForm2() {
	if (errors === 0) {
		gEId('account-creation').submit();
	}
}

// Event listeners

submitBtn.addEventListener('click', submitForm);

// Code to execute

