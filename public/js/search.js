/*eslint-env browser*/

// Variables

var lat = 0;
var lon = 0;

// Functions

String.prototype.replaceAll = function(search, replacement) {
	var target = this;
	return target.split(search).join(replacement);
};

// Check if coordinates are saved in DB
function getPatientLoc() {
	var ajax;
	if (window.XMLHttpRequest) {
		ajax = new XMLHttpRequest();
	}
	else {
		ajax = new ActiveXObject('Microsoft.XMLHTTP');
	}

	ajax.open('POST', '/recherche/localisation', true);
	ajax.setRequestHeader('X-CSRF-TOKEN', getToken());
	ajax.responseType = 'text';
	ajax.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var response = this.responseText;
			var loc = JSON.parse(response);
			lat = loc.latitude;
			lon = loc.longitude;
			// if coordniates equal to 0
			if (lat == 0 && lon == 0) {
				// Get coordiantes from address
				getPatientAddress();

			} else {
				// Display map and search for practitioners
				displayMap(lat, lon);
				getPractitioners(lat, lon);
			}
		}
	}
	ajax.send();
}

// Get patient address in DB
function getPatientAddress() {
	var request;
	if (window.XMLHttpRequest) {
		request = new XMLHttpRequest();
	}
	else {
		request = new ActiveXObject('Microsoft.XMLHTTP');
	}

	request.open('POST', '/recherche/adresse', true);
	request.setRequestHeader('X-CSRF-TOKEN', getToken());
	request.responseType = 'text';
	request.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var response = this.responseText;
			var address = JSON.parse(response);
			var nominatim = "https://nominatim.openstreetmap.org/search?format=json&q=";
			var searchAddress = address.address.replaceAll(' ', '+') + ',' +
			address.zip + '+' +	address.city;
			var locSearch = nominatim + searchAddress;
			httpGet(locSearch);
		}
	}
	request.send();
}

// Convert address into latitude and longitude
function httpGet(url) {
	var xhttp;
	if (window.XMLHttpRequest) {
		xhttp = new XMLHttpRequest();
	}
	else {
		xhttp = new ActiveXObject('Microsoft.XMLHTTP');
	}
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var locInfos = this.responseText;
			locInfos = JSON.parse(locInfos);
			// If address is invalid, show error
			if (locInfos === undefined || locInfos.length === 0) {
				gEId('error').classList.add('visible');

			// Else display map and search for practitioners
			} else {
				lat = locInfos[0].lat;
				lon = locInfos[0].lon;
				displayMap(lat, lon);
				getPractitioners(lat, lon);
			}
		}
	}
	xhttp.open("GET", url, true);
	xhttp.send();
}

// Search for practitioners
function getPractitioners(latitude, longitude) {
	var formData = new FormData;

	formData.append('lat', latitude);
	formData.append('lon', longitude);

	var rqst;
	if (window.XMLHttpRequest) {
		rqst = new XMLHttpRequest();
	}
	else {
		rqst = new ActiveXObject('Microsoft.XMLHTTP');
	}

	rqst.open('POST', '/recherche/praticiens', true);
	rqst.setRequestHeader('X-CSRF-TOKEN', getToken());
	rqst.responseType = 'text';
	rqst.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			var response = this.responseText;
			var practitioners = JSON.parse(response);
			displayPractitioners(practitioners);
		}
	}
	rqst.send(formData);
}

// Display map at location of patient
function displayMap(lat, lon) {
	var map = L.map('map').setView([lat, lon], 14);
	var marker = L.marker([lat, lon]).addTo(map);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox.streets',
		accessToken: 'sk.eyJ1IjoieWthcnRzYWsiLCJhIjoiY2pyN3dscXY5MDBnOTQzcWsxYTlvdnR2bCJ9.BcbDvT8KbJ33HKnHtRM_xQ'
	}).addTo(map);
}

function displayPractitioners(practitioners) {
	// console.log(practitioners);
	for (let i = 0; i < practitioners.length; i++) {
		var practitioner = document.createElement('article');

		// Create title element
		var title = document.createElement('h4');
		var text = practitioners[i].firstname + ' ' + practitioners[i].name;
		var titleContent = document.createTextNode(text);
		title.appendChild(titleContent);
		practitioner.appendChild(title);

		// Create distance element
		var dist = document.createElement('p');
		text = Math.round(practitioners[i].distance * 100) / 100 + ' km';
		distContent = document.createTextNode(text);
		dist.appendChild(distContent);
		practitioner.appendChild(dist);

		var list = document.createElement('ul');

		var services = practitioners[i].services;
		for (let i = 0; i < services.length; i++) {
			var element = document.createElement('li');
			text = services[i].title;
			var elementContent = document.createTextNode(text);
			element.appendChild(elementContent);
			list.appendChild(element);
		}

		practitioner.appendChild(list);

		// Create link to calendar
		var link = document.createElement('a');
		link.setAttribute('href', '/espace-patient/agenda/' +
			practitioners[i].id_practitioner);
		var button = document.createElement('button');
		text = 'Prendre rendez-vous';
		var buttonContent = document.createTextNode(text);
		button.appendChild(buttonContent);
		link.appendChild(button);
		practitioner.appendChild(link);

		gEId('practitioners').appendChild(practitioner);
	}
}

// Code to execute

/*
Check if the patient as coordinates saved in the DB,
if not check if address can yield coordinates.
If the address is invalid display error and propose to get back to profile.
If the coordinates are saved or if the address is valid,
display the map at coordinates location and search for practitioners.
*/
getPatientLoc();
