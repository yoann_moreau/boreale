@extends('default')

@section('title')
	Gestion des prestations
@endsection

@section('content')

<main id="services">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span></p>
	</div>
	<q class="editable">{{ $texts[0] }}</q>
	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	@if (session('type') == 'practitioner')
	<form method="post" action="/espace-praticien/selection-prestations">
		@csrf

		@foreach ($services as $service)
			<h2>
				{{ $service -> title }}
				<input type="checkbox" name="service[]" value="{{ $service -> id_service }}"
				@foreach ($have_services as $h_service)
					@if ($h_service -> id_service === $service -> id_service)
						checked
					@endif
				@endforeach
				>
			</h2>
			@if ($service -> picture !== '')
				<img src="{{ asset('/img/services') }}/{{ $service -> picture }}">
			@endif
			<p>{{ $service -> description }}</p>
			<p>Prix : {{ $service -> price }}</p>
		@endforeach
		@endif
		<input type="submit" name="submit-services" value="Mettre à jour">
	</form>
</main>

@endsection
