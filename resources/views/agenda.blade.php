@extends('default')

@section('title')
	Agenda
@endsection

@section('content')

<div id="overlay" class="overlay">
	<div class="window">
	</div>
</div>

<main id="agenda">
	<div id="user-header">
		<p>Bonjour, <span>{{ session('fname') }}</span> </p>
	</div>

	<q class="editable">{{ $texts[0] }}</q>
	@if (session('type') == 'admin')
		<div id="edit-group0" class="hidden edit-area">
			<textarea class="edit-textarea" name="edit-area0" data-title="0">{{ $texts[0] }}</textarea>
			<button type="button" id='edit-cancel0' class="edit-cancel">annuler</button>
			<button type="button" id="edit-valid0" class="edit-valid">valider</button>
		</div>
		<div id="edit-icon0" class="edit-icon"></div>
	@endif

	<section>
		<header class="mid-page-header">
			<h3 class="editable">{{ $texts[1] }}</h3>
			@if (session('type') == 'admin')
				<div id="edit-group1" class="hidden edit-area">
					<textarea class="edit-textarea" name="edit-area1" data-title="1">{{ $texts[1] }}</textarea>
					<button type="button" id='edit-cancel1' class="edit-cancel">annuler</button>
					<button type="button" id="edit-valid1" class="edit-valid">valider</button>
				</div>
				<div id="edit-icon1" class="edit-icon"></div>
			@endif
		</header>
	</section>

	<section id="diary">
		<header id='diary-header'>
			<div id="date">
			</div>
			<div id="switch">
				@if ($id != false)  <div id="make-appoint">prendre rendez-vous</div> @endif
				<div id="previous" title="semaine précédente">←</div>
				<div id="next" title="semaine suivante">→</div>
			</div>
			<table id="week"></table>
		</header>
		<div id="div-calendar">
<!--			<div id="hours"></div>-->
			<table id="calendar">
			</table>
		</div>
	</section>
</main>
@endsection

@section('scripts')
@if (session('type') === 'patient')
	@if ($id != false)
		<script type="text/javascript" src="{{ asset('/js/agenda-patient.js') }}"></script>
	@else
		<script type="text/javascript" src="{{ asset('/js/agenda-patient-consult.js') }}"></script>
	@endif
@endif
@if (session('type') === 'practitioner')
<script type="text/javascript" src="{{ asset('/js/agenda-practitioner.js') }}"></script>
@endif
@endsection

