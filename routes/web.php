<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ============================================================================
// Routing to pages
// ============================================================================

// Global pages
Route::get('/',                                  	'Controller@home');
Route::get('/confirmation/{link}',               	'Controller@validateAccount');
Route::get('/connexion',                         	'Controller@signin');
Route::get('/contact',                           	'Controller@contact');
Route::get('/deconnexion',                       	'Controller@signout');
Route::get('/faq',                               	'Controller@faq');
Route::get('/informations-patient',              	'Controller@patientInfos');
Route::get('/informations-praticien',            	'Controller@practitionerInfos');
Route::get('/inscription',                       	'Controller@signup');
Route::get('/mentions-legales',                  	'Controller@legal');
Route::get('/oubli',                             	'Controller@forgotPassword');
Route::post('/oubli',                            	'Controller@forgotPassword');
Route::get('/oubli/{link}',                      	'Controller@changePassword');
Route::post('/oubli/{link}',                     	'Controller@changePassword');
Route::get('/partenaires',                       	'Controller@partner');
Route::get('/presentation',                      	'Controller@aboutUs');
Route::get('/validiban',                         	'Controller@isValidIBAN');

// Patient pages
Route::get('/espace-patient',                    	'PatientController@patientArea');
Route::get('/espace-patient/agenda',             	'Controller@agenda');
Route::get('/espace-patient/agenda/{id}',        	'PatientController@agendaPatientMakeAppoint');
Route::get('/espace-patient/profil',             	'Controller@areaProfil');
Route::get('/espace-patient/recherche',          	'PatientController@servicesSearch');

// Practioner pages
Route::get('/espace-praticien',                  	'PractitionerController@practitionerArea');
Route::get('/espace-praticien/agenda',           	'Controller@agenda');
Route::get('/espace-praticien/factures',         	'PractitionerController@practitionerAreaAccounting');
Route::get('/espace-praticien/profil',           	'Controller@areaProfil');
Route::get('/espace-praticien/prestations',      	'Controller@practitionerServices');

// Admin pages
Route::get('/admin-delete',                      	'AdminController@adminDelete');
Route::get('/espace-admin',                      	'AdminController@adminArea');
Route::get('/espace-admin/admins',               	'AdminController@adminAreaAdmins')->name('admins');
Route::get('/espace-admin/creation-admin',       	'AdminController@adminCreate');
Route::get('/espace-admin/editions',             	'AdminController@pagesEdition');
Route::get('/espace-admin/factures',             	'AdminController@adminAreaAccounting');
Route::get('/espace-admin/mails',                	'AdminController@adminMails');
Route::get('/espace-admin/prestations',          	'AdminController@adminServices');
Route::get('/espace-admin/prestations/addition', 	'AdminController@adminAddService');
Route::get('/espace-admin/prestations/edition',  	'AdminController@adminEditService');
Route::get('/espace-admin/profil',               	'Controller@areaProfil');




// ============================================================================
// Processing
// ============================================================================
Route::post('/changePsw',                        				'Controller@updatePsw');
Route::post('/contact/envoi',                    				'Controller@sendMessage');
Route::post('/creation-compte',                  				'Controller@createAccount');
Route::post('/espace-admin/creation-compte-admin', 			'AdminController@createAdmin');
Route::post('/espace-admin/prestations/insertion', 			'AdminController@adminInsertService');
Route::post('/espace-admin/prestations/maj',     				'AdminController@adminUpdateService');
Route::get ('/espace-admin/prestations/suppression',		'AdminController@adminDeleteService');
Route::post('/espace-praticien/selection-prestations',	'PractitionerController@practitionerSelectServices');
Route::post('/toggle-mail-type',                 				'Controller@toggleMail');
Route::post('/update',                           				'Controller@updateInfo');

// ============================================================================
// Ajax
// ============================================================================
Route::post('/ajaxChangePsw',                    	'Controller@ajaxUpdatePsw');
Route::post('/get-appoints-pat',                 	'PatientController@getAppointsPatient');
Route::post('/get-unavailability',         			 	'PatientController@getUnavailability');
Route::post('/get-appoints-pract',               	'PractitionerController@getAppointsPractitioner');
Route::post('/recherche/adresse',                	'PatientController@searchAddress');
Route::post('/recherche/localisation',           	'PatientController@searchLoc');
Route::post('/recherche/praticiens',             	'PatientController@searchPractitioners');
Route::post('/update-mail',                      	'Controller@updateMail');
Route::post('/update-text',                      	'AdminController@updateText');
Route::post('/verification-email',               	'Controller@existEmail');
Route::post('/verification-titre',               	'Controller@existTitle');
